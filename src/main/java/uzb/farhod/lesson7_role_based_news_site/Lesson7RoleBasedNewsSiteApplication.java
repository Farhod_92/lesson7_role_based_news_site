package uzb.farhod.lesson7_role_based_news_site;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Lesson7RoleBasedNewsSiteApplication {

    public static void main(String[] args) {
        SpringApplication.run(Lesson7RoleBasedNewsSiteApplication.class, args);
    }

}

