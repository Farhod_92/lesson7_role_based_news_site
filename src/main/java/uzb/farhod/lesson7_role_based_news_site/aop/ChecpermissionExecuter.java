package uzb.farhod.lesson7_role_based_news_site.aop;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import uzb.farhod.lesson7_role_based_news_site.entity.User;
import uzb.farhod.lesson7_role_based_news_site.exceptions.ForbiddenUserExecption;

@Component
@Aspect
public class ChecpermissionExecuter {
    @Before(value = "@annotation(checkPermission)")
    public void checkUserPermissionMyMethod(CheckPermission checkPermission){
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        boolean exists=false;
        for (GrantedAuthority authority : user.getAuthorities()) {
            if(authority.getAuthority().equals(checkPermission.huquq())) {
                exists = true;
                break;
            }
        }

        if(!exists)
            throw new ForbiddenUserExecption(checkPermission.huquq(), "ruxsat yo'q");
    }
}
