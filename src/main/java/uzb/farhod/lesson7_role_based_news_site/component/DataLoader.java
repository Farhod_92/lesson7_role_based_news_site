package uzb.farhod.lesson7_role_based_news_site.component;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import uzb.farhod.lesson7_role_based_news_site.entity.Role;
import uzb.farhod.lesson7_role_based_news_site.entity.User;
import uzb.farhod.lesson7_role_based_news_site.entity.enums.Permission;
import uzb.farhod.lesson7_role_based_news_site.repository.RoleRepository;
import uzb.farhod.lesson7_role_based_news_site.repository.UserRepository;
import uzb.farhod.lesson7_role_based_news_site.utils.AppConstants;

import java.util.Arrays;

import static uzb.farhod.lesson7_role_based_news_site.entity.enums.Permission.*;

@Component
public class DataLoader implements CommandLineRunner {
    @Autowired
    UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Value("${spring.sql.init.mode}")
    String initMode;

    @Override
    public void run(String... args) throws Exception {
        if(initMode.equals("always")){
            Permission[] permissions = Permission.values();

            Role admin = roleRepository.save(new Role(
                    AppConstants.ADMIN,
                    Arrays.asList(permissions),
                    "sistema egasi"
            ));

            Role user = roleRepository.save(new Role(
                    AppConstants.USER,
                    Arrays.asList(ADD_COMMENT, EDIT_COMMENT, DELETE_MY_COMMENT, EDIT_MY_COMMENT ),
                    "oddiy user"
            ));

            userRepository.save(new User(
                    "Admin",
                    "admin",
                    passwordEncoder.encode("123"),
                    admin,
                    true
            ));

            userRepository.save(new User(
                    "User",
                    "user",
                    passwordEncoder.encode("123"),
                    user,
                    true
            ));

        }
    }
}
