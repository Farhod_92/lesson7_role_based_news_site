package uzb.farhod.lesson7_role_based_news_site.config;

import org.springframework.data.domain.AuditorAware;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import uzb.farhod.lesson7_role_based_news_site.entity.User;

import java.util.Optional;
import java.util.UUID;

public class Audit implements AuditorAware<User> {
    @Override
    public Optional<User> getCurrentAuditor() {
        Authentication authentication= SecurityContextHolder.getContext().getAuthentication();
        if(authentication!=null
        && authentication.isAuthenticated()
        && !authentication.getPrincipal().equals("anonymousUser")){
            User user =(User) authentication.getPrincipal();
            return Optional.of(user);
        }
        return Optional.empty();
    }
}
