package uzb.farhod.lesson7_role_based_news_site.config;

import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.stereotype.Component;
import uzb.farhod.lesson7_role_based_news_site.entity.User;

import java.util.UUID;

@Component
@EnableJpaAuditing
public class AuditConfig {

    @Bean
    AuditorAware<User> auditorAware(){
        return new Audit();
    }
}
