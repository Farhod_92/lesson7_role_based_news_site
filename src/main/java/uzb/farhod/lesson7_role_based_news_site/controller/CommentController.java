package uzb.farhod.lesson7_role_based_news_site.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import uzb.farhod.lesson7_role_based_news_site.aop.CheckPermission;
import uzb.farhod.lesson7_role_based_news_site.entity.Comment;
import uzb.farhod.lesson7_role_based_news_site.entity.Post;
import uzb.farhod.lesson7_role_based_news_site.payload.ApiResponse;
import uzb.farhod.lesson7_role_based_news_site.payload.CommentDto;
import uzb.farhod.lesson7_role_based_news_site.payload.PostDto;
import uzb.farhod.lesson7_role_based_news_site.service.CommentService;
import uzb.farhod.lesson7_role_based_news_site.service.PostService;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/comment")
public class CommentController {

    @Autowired
    CommentService commentService;

    @CheckPermission(huquq = "ADD_COMMENT")
    @PostMapping("/add")
    public ResponseEntity<?> addComment(@Valid @RequestBody CommentDto commentDto){
        ApiResponse apiResponse = commentService.addComment(commentDto);
        return  ResponseEntity.status(apiResponse.isSuccess()?200:409).body(apiResponse);
    }

    @GetMapping
    public ResponseEntity<?> viewComments(){
        List<Comment> userList = commentService.getComents();
        return  ResponseEntity.ok(userList);
    }

    @PreAuthorize(value = "hasAuthority('EDIT_COMMENT')")
    @PutMapping("/{id}")
    public ResponseEntity<?> editComment(@Valid @RequestBody CommentDto commentDto, @PathVariable Long id){
        ApiResponse apiResponse = commentService.editComment(commentDto,id);
        return  ResponseEntity.status(apiResponse.isSuccess()?200:409).body(apiResponse);
    }

    @PreAuthorize(value = "hasAuthority('EDIT_MY_COMMENT')")
    @PostMapping("/my/{id}")
    public ResponseEntity<?> editMyComment(@Valid @RequestBody CommentDto commentDto, @PathVariable Long id){
        ApiResponse apiResponse = commentService.editMyComment(commentDto,id);
        return  ResponseEntity.status(apiResponse.isSuccess()?200:409).body(apiResponse);
    }

    @PreAuthorize(value = "hasAuthority('DELETE_COMMENT')")
    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteComment(@PathVariable Long id){
        ApiResponse apiResponse = commentService.deleteComment(id);
        return  ResponseEntity.status(apiResponse.isSuccess()?204:409).body(apiResponse);
    }

    @PreAuthorize(value = "hasAuthority('DELETE_MY_COMMENT')")
    @DeleteMapping("/my/{id}")
    public ResponseEntity<?> deleteMyComment(@PathVariable Long id){
        ApiResponse apiResponse = commentService.deleteMyComment(id);
        return  ResponseEntity.status(apiResponse.isSuccess()?204:409).body(apiResponse);
    }

}
