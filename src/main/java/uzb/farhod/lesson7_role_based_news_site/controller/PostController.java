package uzb.farhod.lesson7_role_based_news_site.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import uzb.farhod.lesson7_role_based_news_site.aop.CheckPermission;
import uzb.farhod.lesson7_role_based_news_site.entity.Post;
import uzb.farhod.lesson7_role_based_news_site.entity.User;
import uzb.farhod.lesson7_role_based_news_site.payload.ApiResponse;
import uzb.farhod.lesson7_role_based_news_site.payload.PostDto;
import uzb.farhod.lesson7_role_based_news_site.payload.UserDto;
import uzb.farhod.lesson7_role_based_news_site.service.PostService;
import uzb.farhod.lesson7_role_based_news_site.service.UserService;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/post")
public class PostController {

    @Autowired
    PostService postService;

    @CheckPermission(huquq = "ADD_POST")
    @PostMapping("/add")
    public ResponseEntity<?> addrUser(@Valid @RequestBody PostDto postDto){
        ApiResponse apiResponse = postService.addPost(postDto);
        return  ResponseEntity.status(apiResponse.isSuccess()?200:409).body(apiResponse);
    }

    @GetMapping
    public ResponseEntity<?> viewPosts(){
        List<Post> userList = postService.getPosts();
        return  ResponseEntity.ok(userList);
    }


    @PreAuthorize(value = "hasAuthority('EDIT_POST')")
    @PostMapping("/edit/{id}")
    public ResponseEntity<?> editPost(@Valid @RequestBody PostDto postDto, @PathVariable Long id){
        ApiResponse apiResponse = postService.editPost(postDto,id);
        return  ResponseEntity.status(apiResponse.isSuccess()?200:409).body(apiResponse);
    }

    @PreAuthorize(value = "hasAuthority('DELETE_POST')")
    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteUser(@PathVariable Long id){
        ApiResponse apiResponse = postService.deletePost(id);
        return  ResponseEntity.status(apiResponse.isSuccess()?204:409).body(apiResponse);
    }

}
