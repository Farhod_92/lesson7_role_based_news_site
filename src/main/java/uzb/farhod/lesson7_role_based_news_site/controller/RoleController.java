package uzb.farhod.lesson7_role_based_news_site.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import uzb.farhod.lesson7_role_based_news_site.aop.CheckPermission;
import uzb.farhod.lesson7_role_based_news_site.entity.Role;
import uzb.farhod.lesson7_role_based_news_site.payload.ApiResponse;
import uzb.farhod.lesson7_role_based_news_site.payload.RoleDto;
import uzb.farhod.lesson7_role_based_news_site.service.RoleService;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/role")
public class RoleController {
    @Autowired
    RoleService roleService;

    @PreAuthorize(value = "hasAuthority('ADD_ROLE')")
    @PostMapping
    public ResponseEntity<?> addRole(@Valid @RequestBody RoleDto roleDto){
        ApiResponse apiResponse = roleService.addRole(roleDto);
        return  ResponseEntity.status(apiResponse.isSuccess()?200:409).body(apiResponse);
    }

   // @PreAuthorize(value = "hasAuthority('EDIT_ROLE')")
    @CheckPermission(huquq = "EDIT_ROLE")
    @PutMapping("/{id}")
    public ResponseEntity<?> editRole(@PathVariable Long id, @Valid @RequestBody RoleDto roleDto){
        ApiResponse apiResponse = roleService.editRole(id,roleDto);
        return  ResponseEntity.status(apiResponse.isSuccess()?200:409).body(apiResponse);
    }

    @CheckPermission(huquq = "DELETE_ROLE")
    @DeleteMapping("/{id}")
    public ResponseEntity<?> editRole(@PathVariable Long id){
        ApiResponse apiResponse = roleService.deleteRole(id);
        return  ResponseEntity.status(apiResponse.isSuccess()?200:409).body(apiResponse);
    }

    @CheckPermission(huquq = "VIEW_ROLES")
    @GetMapping
    public ResponseEntity<?> getRoles(){
        List<Role> roles = roleService.getRoles();
        return  ResponseEntity.ok(roles);
    }



}
