package uzb.farhod.lesson7_role_based_news_site.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import uzb.farhod.lesson7_role_based_news_site.aop.CheckPermission;
import uzb.farhod.lesson7_role_based_news_site.entity.User;
import uzb.farhod.lesson7_role_based_news_site.payload.ApiResponse;
import uzb.farhod.lesson7_role_based_news_site.payload.RegisterDto;
import uzb.farhod.lesson7_role_based_news_site.payload.UserDto;
import uzb.farhod.lesson7_role_based_news_site.service.AuthService;
import uzb.farhod.lesson7_role_based_news_site.service.UserService;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/user")
public class UserController {

    @Autowired
    UserService userService;

    @CheckPermission(huquq = "ADD_USER")
    @PostMapping("/add")
    public ResponseEntity<?> addrUser(@Valid @RequestBody UserDto userDto){
        ApiResponse apiResponse = userService.addUser(userDto);
        return  ResponseEntity.status(apiResponse.isSuccess()?200:409).body(apiResponse);
    }

    @PreAuthorize(value = "hasAuthority('VIEW_USERS')")
    @GetMapping
    public ResponseEntity<?> viewUsers(){
        List<User> userList = userService.getUsers();
        return  ResponseEntity.ok(userList);
    }


    @PreAuthorize(value = "hasAuthority('EDIT_USER')")
    @PostMapping("/edit/{id}")
    public ResponseEntity<?> editUser(@Valid @RequestBody UserDto userDto, @PathVariable Long id){
        ApiResponse apiResponse = userService.editUser(userDto,id);
        return  ResponseEntity.status(apiResponse.isSuccess()?200:409).body(apiResponse);
    }

    @PreAuthorize(value = "hasAuthority('DELETE_USER')")
    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteUser(@PathVariable Long id){
        ApiResponse apiResponse = userService.deleteUser(id);
        return  ResponseEntity.status(apiResponse.isSuccess()?204:409).body(apiResponse);
    }

}
