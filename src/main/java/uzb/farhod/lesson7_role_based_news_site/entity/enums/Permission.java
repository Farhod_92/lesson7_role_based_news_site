package uzb.farhod.lesson7_role_based_news_site.entity.enums;

public enum Permission {
    ADD_USER,
    EDIT_USER,
    DELETE_USER,
    VIEW_USERS,
    ADD_ROLE,
    EDIT_ROLE,
    DELETE_ROLE,
    VIEW_ROLES,
    ADD_POST,
    EDIT_POST,
    DELETE_POST,
    ADD_COMMENT,
    EDIT_COMMENT,
    EDIT_MY_COMMENT,
    DELETE_MY_COMMENT,
    DELETE_COMMENT
}
