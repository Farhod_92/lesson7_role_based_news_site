package uzb.farhod.lesson7_role_based_news_site.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ControllerExceptionHandler {

    @ExceptionHandler(ForbiddenUserExecption.class)
    public ResponseEntity<?> ForbiddenUserExecption(ForbiddenUserExecption ex){
        ExceptionMessage message=new ExceptionMessage(ex.getType(), ex.getMessage(), null);
        return new ResponseEntity<>(message, HttpStatus.FORBIDDEN);
    }

    @ExceptionHandler(ResourceNotFoundException.class)
    public ResponseEntity<?> ResourceNotFoundException(ResourceNotFoundException ex){
        ExceptionMessage message=new ExceptionMessage(
                ex.getResourceField(),
                ex.getMessage(),
                ex.getObject());
        return new ResponseEntity<>(message, HttpStatus.FORBIDDEN);
    }
}
