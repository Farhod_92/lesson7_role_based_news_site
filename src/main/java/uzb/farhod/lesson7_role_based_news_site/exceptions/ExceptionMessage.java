package uzb.farhod.lesson7_role_based_news_site.exceptions;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class ExceptionMessage {
    private String type;
    private String message;
    private Object object;
}
