package uzb.farhod.lesson7_role_based_news_site.exceptions;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@EqualsAndHashCode(callSuper = true)
@ResponseStatus(HttpStatus.FORBIDDEN)
@Data
public class ForbiddenUserExecption extends RuntimeException {
   private String type;
   private String message;

    public ForbiddenUserExecption(String type, String message) {
        this.type = type;
        this.message = message;
    }
}
