package uzb.farhod.lesson7_role_based_news_site.exceptions;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
@AllArgsConstructor
@Data
public class ResourceNotFoundException extends RuntimeException {
    private final String resourceName; //roleEntity
    private final String resourceField; // name
    private final Object object; // USER, ADMIn

}
