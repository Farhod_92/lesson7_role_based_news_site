package uzb.farhod.lesson7_role_based_news_site.payload;

import lombok.Getter;

import javax.validation.constraints.NotNull;

@Getter
public class CommentDto {

    @NotNull
    private String text;

    @NotNull(message = "password bo'sh keldi!")
    private Long postId;
}
