package uzb.farhod.lesson7_role_based_news_site.payload;

import lombok.Getter;

import javax.validation.constraints.NotNull;

@Getter
public class RegisterDto {

    @NotNull
    private String fullName;

    @NotNull
    private String username;

    @NotNull(message = "password bo'sh keldi!")
    private String password;

    @NotNull
    private String prePassword;
}
