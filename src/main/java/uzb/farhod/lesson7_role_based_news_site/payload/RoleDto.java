package uzb.farhod.lesson7_role_based_news_site.payload;

import lombok.Getter;
import uzb.farhod.lesson7_role_based_news_site.entity.enums.Permission;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

@Getter
public class RoleDto {
    @NotBlank
    private String name;

    private String description;

    @NotEmpty
    private List<Permission> permissionList;

}
