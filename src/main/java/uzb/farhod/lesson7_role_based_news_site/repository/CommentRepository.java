package uzb.farhod.lesson7_role_based_news_site.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uzb.farhod.lesson7_role_based_news_site.entity.Comment;

public interface CommentRepository extends JpaRepository<Comment, Long > {
}
