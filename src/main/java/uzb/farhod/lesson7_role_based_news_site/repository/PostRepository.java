package uzb.farhod.lesson7_role_based_news_site.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uzb.farhod.lesson7_role_based_news_site.entity.Post;
import uzb.farhod.lesson7_role_based_news_site.entity.Role;

import java.util.Optional;

public interface PostRepository extends JpaRepository<Post, Long > {
}
