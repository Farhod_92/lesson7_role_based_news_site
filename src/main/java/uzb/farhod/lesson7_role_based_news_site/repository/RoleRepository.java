package uzb.farhod.lesson7_role_based_news_site.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uzb.farhod.lesson7_role_based_news_site.entity.Role;
import uzb.farhod.lesson7_role_based_news_site.entity.User;

import java.util.Optional;

public interface RoleRepository extends JpaRepository<Role, Long > {
    Optional<Role> findByName(String name);
    boolean existsByName(String name);
}
