package uzb.farhod.lesson7_role_based_news_site.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uzb.farhod.lesson7_role_based_news_site.entity.User;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long > {
    boolean existsByUsername(String username);
    boolean existsByUsernameAndIdNot(String username, Long id);
    Optional<User> findByUsername(String username);
}
