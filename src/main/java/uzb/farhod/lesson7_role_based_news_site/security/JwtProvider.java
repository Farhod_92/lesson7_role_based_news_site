package uzb.farhod.lesson7_role_based_news_site.security;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.stereotype.Component;
import uzb.farhod.lesson7_role_based_news_site.entity.Role;

import java.util.Date;
import java.util.Set;

@Component
public class JwtProvider {
    private String secretKey="key dsdsadsadsadsadasdasdasdasdasdasdsadsa";
    private long expireAfter=1000*60*60*24*2;

    public String generateToken(String username, Role role){
        String token = Jwts.builder()
                .signWith(SignatureAlgorithm.HS512, secretKey)
                .setSubject(username)
                .setIssuedAt(new Date())
                .setExpiration(new Date(System.currentTimeMillis() + expireAfter))
                .claim("role", role.getName())
                .compact();
        return  token;
    }

    public String getUsernameFromToken(String token){
        String username = Jwts.parser()
                .setSigningKey(secretKey)
                .parseClaimsJws(token)
                .getBody()
                .getSubject();
        return  username;
    }

}
