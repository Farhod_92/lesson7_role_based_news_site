package uzb.farhod.lesson7_role_based_news_site.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import uzb.farhod.lesson7_role_based_news_site.entity.User;
import uzb.farhod.lesson7_role_based_news_site.exceptions.ResourceNotFoundException;
import uzb.farhod.lesson7_role_based_news_site.payload.ApiResponse;
import uzb.farhod.lesson7_role_based_news_site.payload.RegisterDto;
import uzb.farhod.lesson7_role_based_news_site.repository.RoleRepository;
import uzb.farhod.lesson7_role_based_news_site.repository.UserRepository;
import uzb.farhod.lesson7_role_based_news_site.utils.AppConstants;

import java.util.Optional;

@Service
public class AuthService implements UserDetailsService {
    @Autowired
    UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    public ApiResponse registerUser(RegisterDto registerDto){
        if(!registerDto.getPassword().equals(registerDto.getPrePassword()))
            return new ApiResponse("parollar mos emas", false);

        if(userRepository.existsByUsername(registerDto.getUsername()))
            return new ApiResponse("bu user bor", false);

        User user=new User(
                registerDto.getFullName(),
                registerDto.getUsername(),
                passwordEncoder.encode(registerDto.getPassword()),
                roleRepository.findByName(AppConstants.USER)
                        .orElseThrow(()->new ResourceNotFoundException("role","name",AppConstants.USER)),
                true
        );
        userRepository.save(user);
        return  new ApiResponse("user regitratsiyadan o'tdi", true);
    }

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        Optional<User> optionalUser = userRepository.findByUsername(s);
        return optionalUser.orElseThrow(()->new UsernameNotFoundException("user topilmadi"));
    }
}
