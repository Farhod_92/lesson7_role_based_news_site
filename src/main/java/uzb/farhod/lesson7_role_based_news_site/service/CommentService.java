package uzb.farhod.lesson7_role_based_news_site.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import uzb.farhod.lesson7_role_based_news_site.entity.Comment;
import uzb.farhod.lesson7_role_based_news_site.entity.Post;
import uzb.farhod.lesson7_role_based_news_site.entity.User;
import uzb.farhod.lesson7_role_based_news_site.exceptions.ResourceNotFoundException;
import uzb.farhod.lesson7_role_based_news_site.payload.ApiResponse;
import uzb.farhod.lesson7_role_based_news_site.payload.CommentDto;
import uzb.farhod.lesson7_role_based_news_site.payload.PostDto;
import uzb.farhod.lesson7_role_based_news_site.repository.CommentRepository;
import uzb.farhod.lesson7_role_based_news_site.repository.PostRepository;
import uzb.farhod.lesson7_role_based_news_site.repository.UserRepository;

import java.util.List;
import java.util.Optional;

@Service
public class CommentService {
    @Autowired
    CommentRepository commentRepository;

    @Autowired
    PostRepository postRepository;

    public ApiResponse addComment(CommentDto commentDto) {
        Comment comment=new Comment();
        comment.setText(commentDto.getText());
        comment.setPost(
                postRepository.findById(commentDto.getPostId())
                        .orElseThrow(()->new ResourceNotFoundException("post","id",commentDto.getPostId()))
        );
        commentRepository.save(comment);
        return  new ApiResponse("comment qo'shildi", true);
    }

    public ApiResponse editComment(CommentDto commentDto, Long id) {
        Optional<Comment> op = commentRepository.findById(id);
        if(!op.isPresent())
            return new ApiResponse("comment topilmadi", false);

        Comment comment= op.get();
        comment.setText(commentDto.getText());
        commentRepository.save(comment);
        return  new ApiResponse("comment tahrirlandi", true);
    }

    public List<Comment> getComents() {
        List<Comment> all = commentRepository.findAll();
        return  all;
    }

    public ApiResponse deleteComment(Long id) {
       try {
            commentRepository.deleteById(id);
            return new ApiResponse("comment o'chirildi", true);
        }catch (Exception e){
            return new ApiResponse("comment o'chirilmadii", false);
       }
    }

    public ApiResponse deleteMyComment(Long id) {
            Optional<Comment> byId = commentRepository.findById(id);
            if(!byId.isPresent())
                return new ApiResponse("comment topilmadi", false);

            Comment comment = byId.get();
            User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            if(!user.getId().equals(comment.getCreatedBy().getId()))
                return new ApiResponse("siz by kommentni o'chirolmaysiz", false);

        try {
            commentRepository.deleteById(id);
            return new ApiResponse("comment o'chirildi", true);
        }catch (Exception e){
            return new ApiResponse("comment o'chirilmadii", false);
        }
    }

    public ApiResponse editMyComment(CommentDto commentDto, Long id) {

        Optional<Comment> op = commentRepository.findById(id);
        if(!op.isPresent())
            return new ApiResponse("comment topilmadi", false);
        Comment comment= op.get();

        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(!user.getId().equals(comment.getCreatedBy().getId()))
            return new ApiResponse("siz by kommentni tahrirlay olmaysiz", false);

        comment.setText(commentDto.getText());
        commentRepository.save(comment);
        return  new ApiResponse("comment tahrirlandi", true);
    }
}
