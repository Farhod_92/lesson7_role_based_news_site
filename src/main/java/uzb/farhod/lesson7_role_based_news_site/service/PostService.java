package uzb.farhod.lesson7_role_based_news_site.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import uzb.farhod.lesson7_role_based_news_site.entity.Post;
import uzb.farhod.lesson7_role_based_news_site.entity.User;
import uzb.farhod.lesson7_role_based_news_site.exceptions.ResourceNotFoundException;
import uzb.farhod.lesson7_role_based_news_site.payload.ApiResponse;
import uzb.farhod.lesson7_role_based_news_site.payload.PostDto;
import uzb.farhod.lesson7_role_based_news_site.payload.UserDto;
import uzb.farhod.lesson7_role_based_news_site.repository.PostRepository;
import uzb.farhod.lesson7_role_based_news_site.repository.RoleRepository;
import uzb.farhod.lesson7_role_based_news_site.repository.UserRepository;

import java.util.List;
import java.util.Optional;

@Service
public class PostService {

    @Autowired
    PostRepository postRepository;

    public ApiResponse addPost(PostDto postDto) {
        Post post =new Post(postDto.getTitle(), postDto.getText(), postDto.getUrl());
        postRepository.save(post);
        return  new ApiResponse("post qo'shildi", true);
    }

    public ApiResponse editPost(PostDto postDto, Long id) {
        Optional<Post> op = postRepository.findById(id);
        if(!op.isPresent())
            return new ApiResponse("post topilmadi", false);
        Post post= op.get();
        post.setText(postDto.getText());
        post.setTitle(postDto.getTitle());
        post.setUrl(postDto.getUrl());
        postRepository.save(post);
        return  new ApiResponse("post tahrirlandi", true);
    }

    public List<Post> getPosts() {
        List<Post> all = postRepository.findAll();
        return  all;
    }

    public ApiResponse deletePost(Long id) {
       try {
            postRepository.deleteById(id);
            return new ApiResponse("post o'chirildi", true);
        }catch (Exception e){
            return new ApiResponse("post o'chirilmadii", false);
       }
    }
}
