package uzb.farhod.lesson7_role_based_news_site.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uzb.farhod.lesson7_role_based_news_site.entity.Role;
import uzb.farhod.lesson7_role_based_news_site.payload.ApiResponse;
import uzb.farhod.lesson7_role_based_news_site.payload.RoleDto;
import uzb.farhod.lesson7_role_based_news_site.repository.RoleRepository;

import java.util.List;

@Service
public class RoleService {
    @Autowired
    RoleRepository roleRepository;

    public ApiResponse addRole(RoleDto roleDto) {
        if(roleRepository.existsByName(roleDto.getName()))
            return new ApiResponse("bunaqa rol bor", false);

        Role role=new Role(
                roleDto.getName(),
                roleDto.getPermissionList(),
                roleDto.getDescription()
        );
        roleRepository.save(role);
        return new ApiResponse("role saqlandi", true);
    }

    public ApiResponse editRole(Long id, RoleDto roleDto) {
        return null;
    }

    public ApiResponse deleteRole(Long id) {
        roleRepository.deleteById(id);
        return new ApiResponse("role o'chirildi", true);
    }

    public List<Role> getRoles() {
        return roleRepository.findAll();
    }
}
