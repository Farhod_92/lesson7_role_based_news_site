package uzb.farhod.lesson7_role_based_news_site.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import uzb.farhod.lesson7_role_based_news_site.entity.Role;
import uzb.farhod.lesson7_role_based_news_site.entity.User;
import uzb.farhod.lesson7_role_based_news_site.exceptions.ResourceNotFoundException;
import uzb.farhod.lesson7_role_based_news_site.payload.ApiResponse;
import uzb.farhod.lesson7_role_based_news_site.payload.UserDto;
import uzb.farhod.lesson7_role_based_news_site.repository.RoleRepository;
import uzb.farhod.lesson7_role_based_news_site.repository.UserRepository;
import uzb.farhod.lesson7_role_based_news_site.utils.AppConstants;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    RoleRepository roleRepository;

    public ApiResponse addUser(UserDto userDto) {

        if(userRepository.existsByUsername(userDto.getUsername()))
            return new ApiResponse("bu user bor", false);

        User user=new User(
                userDto.getFullName(),
                userDto.getUsername(),
                passwordEncoder.encode(userDto.getPassword()),
                roleRepository.findById(userDto.getRoleId())
                        .orElseThrow(()->new ResourceNotFoundException("role", "id",userDto.getRoleId())),
                true
        );
        userRepository.save(user);
        return  new ApiResponse("user regitratsiyadan o'tdi", true);
    }

    public ApiResponse editUser(UserDto userDto, Long id) {
        if(userRepository.existsByUsernameAndIdNot(userDto.getUsername(),id))
            return new ApiResponse("bu user bor", false);

        User user= userRepository.getById(id);
        user.setFullName(userDto.getFullName());
        user.setUsername(userDto.getUsername());
        user.setPassword(passwordEncoder.encode(userDto.getPassword()));
        user.setRole( roleRepository.findById(userDto.getRoleId())
                .orElseThrow(() -> new ResourceNotFoundException("role", "id", userDto.getRoleId())));
        user.setEnabled(userDto.isEnabled());
        userRepository.save(user);
        return  new ApiResponse("user tahrirlandi", true);
    }

    public List<User> getUsers() {
        List<User> all = userRepository.findAll();
        return  all;
    }

    public ApiResponse deleteUser(Long id) {
       try {
            userRepository.deleteById(id);
            return new ApiResponse("user o'chirildi", true);
        }catch (Exception e){
            return new ApiResponse("user o'chirilmadii", false);
       }
    }
}
